<html>
	<head>
		<title>Add Product</title>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" crossorigin="anonymous">
	</head>
	<body>
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
			<div class="container">
				<a class="navbar-brand" href="#">Navbar</a>
				
				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					<ul class="navbar-nav mr-auto">
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								Product
							</a>
							<div class="dropdown-menu" aria-labelledby="navbarDropdown">
								<a class="dropdown-item" href="#">Add Product</a>
								<a class="dropdown-item" href="#">View Product</a>
								
							</div>
						</li>
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								Category
							</a>
							<div class="dropdown-menu" aria-labelledby="navbarDropdown">
								<a class="dropdown-item" href="{{ route('category.create') }}">Add Category</a>
								<a class="dropdown-item" href="{{ route('category.index') }}">View Category</a>
								
							</div>
						</li>
					</ul>
				</div>
			</div>
		</nav>
		<div class="container">
			<div class="row justify-content-md-center" >
				<div class="col-md-8" style="margin-top: 55px;border: 1px solid;">
					<h3 style="padding-top: 11px;">Add Product</h3>
					<hr>
					@if (isset($edit))
							<form action="{{ route('product.update',$prod->id) }}" method="POST">
								<input type="hidden" name="_method" value="PUT">
								@else
					<form method="POST" action="{{ route('product.store') }}">
						@endif
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<div class="form-row">
							<div class="col">
								<div class="form-group">
									

									<label  class="mr-sm-4" for="inlineFormCustomSelect">Category Id</label>
									<select name="category_id" class="custom-select mr-sm-4" id="inlineFormCustomSelect" placeholder="Choose" required value="{{ isset($edit) ? $prod->id : null}}">

										{{-- foreach(\App\Category::all() as $c) --}}
										@foreach($prod as $p)
										
										<option value="{{ $p->category_id }}">{{ $p->name }}</option>

									@endforeach
									</select>
								</div>
							</div>
							<div class="col">
								<div class="form-group">
									<label class="mr-sm-4" for="inlineFormCustomSelect">Status</label>
									<select name="status" class="custom-select mr-sm-4" id="inlineFormCustomSelect" required value="{{ isset($edit) ? $prod->status : null}}">
										<!-- <option selected>Choose...</option> -->
										<option value="1">Avalable</option>
										<option selected value="0">Unavalable</option>
									</select>
								</div>
							</div>
						</div>
						<div class="form-row">
							<div class="col">
								<div class="form-group">
									<label for="formGroupExampleInput2">Product Name</label>
									<input name="name" type="text" class="form-control" id="formGroupExampleInput2" placeholder="Product Name" required value="{{ isset($edit) ? $prod->name : null}}">
								</div>
							</div>
							<div class="col">
								<div class="form-group">
									<label for="formGroupExampleInput2">Image</label>
									<input name="image" type="file" class="form-control" id="formGroupExampleInput2" placeholder="Image" required value="{{ isset($edit) ? $prod->image : null}}">
								</div>
							</div>
						</div>
						<div class="form-group">
							<label for="exampleFormControlTextarea1">Description</label>
							<textarea name="desc" class="form-control" id="exampleFormControlTextarea1" rows="3" required value="{{ isset($edit) ? $prod->desk : null}}"></textarea>
						</div>
					
					<div class="form-group">
						<button type="submit" class="btn btn-success btn-block">Submit</button>
					</div>
					</form>
				</div>
			</div>
		</div>
		<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
	</body>
</html>