<!DOCTYPE html>
<html>
	<head>
		<title>View Categories</title>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" crossorigin="anonymous">
	</head>
	<body>
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
			<div class="container">
				<a class="navbar-brand" href="#">Navbar</a>
			</div>
		</nav>
		<div class="container">
			<a href="#" class="btn btn-primary">Add Product</a>
			<div class="row justify-content-md-center" >
				<div class="col-md-8" style="margin-top: 97px;">
					<table class="table" style="border: 2px solid;">
						<thead>
							<tr>
								<th scope="col">ID</th>
								<th scope="col">Name</th>
								<th scope="col">Image</th>
								<th scope="col">Discription</th>
								<th scope="col">Status</th>
								<th scope="col">Action</th>
							</tr>
						</thead>
						<tbody>
							@foreach($prod as $p)
							<tr>
								<th scope="row">{{$p->category_id}}</th>
								<td>{{$p->name}}</td>
								<td>{{$p->image}}</td>
								<td>{{$p->desc}}</td>
								<td>{{$p->status}}</td>
								<td><a href="{{ route('product.show',$p->id) }}" class="btn btn-primary">Show</a>
									<a href="{{ route('product.edit',$p->id) }}" class="btn btn-warning">Edit</a></td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</body>
</html>