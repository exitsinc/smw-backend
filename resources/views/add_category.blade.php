<!DOCTYPE html>
<html>
	<head>
		<title>Add Category</title>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" crossorigin="anonymous">
	</head>
	<body>
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
			<div class="container">
				<a class="navbar-brand" href="#">Navbar</a>
				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					<ul class="navbar-nav mr-auto">
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								Product
							</a>
							<div class="dropdown-menu" aria-labelledby="navbarDropdown">
								<a class="dropdown-item" href="#">Add Product</a>
								<a class="dropdown-item" href="#">View Product</a>
								
							</div>
						</li>
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								Category
							</a>
							<div class="dropdown-menu" aria-labelledby="navbarDropdown">
								<a class="dropdown-item" href="{{ route('category.create') }}">Add Category</a>
								<a class="dropdown-item" href="{{ route('category.index') }}">View Category</a>
								
							</div>
						</li>
					</ul>
				</div>
			</div>
		</nav>
		<div class="container">
			<div class="row justify-content-md-center" >
				<div class="col-md-4">
					<div class="card" style="
						margin-top: 50px;
						padding: 20px;"
						>
						<div class="card-body">
							<div class="row justify-content-md-center">
								<h3>{{ isset($edit) ? 'Edit' : 'Add'}} Category</h3>
							</div>
							@if (isset($edit))
							<form action="{{ route('category.update',$cat->id) }}" method="POST">
								<input type="hidden" name="_method" value="PUT">
								@else
								<form action="{{ route('category.store') }}" method="POST">
									@endif
									
									<input type="hidden" name="_token" value="{{ csrf_token() }}">
									<div class="row" style="
										margin-bottom: 10px;">
										<input type="text" name="name" id="title" class="form-control" maxlength="50" autocomplet="off" placeholder="Category" required value="{{ isset($edit) ? $cat->name : null}}"></input>
									</div>
									<div class="row">
										<button type="submit" class="btn btn-success btn-block">Submit</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
			<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
			<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
		</body>
	</html>