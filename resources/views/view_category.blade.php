<!DOCTYPE html>
<html>
	<head>
		<title>View Categories</title>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" crossorigin="anonymous">
	</head>
	<body>
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
			<div class="container">
				<a class="navbar-brand" href="#">Navbar</a>
			</div>
		</nav>
		<div class="container">
			<a href="{{ route('category.create') }}" class="btn btn-primary">Add Category</a>
			<div class="row justify-content-md-center" >
				<div class="col-md-8" style="margin-top: 97px;">
					<table class="table" style="border: 2px solid;">
						<thead>
							<tr>
								<th scope="col">ID</th>
								<th scope="col">Name</th>
								<th scope="col">DATE</th>
								<th scope="col">ACTION</th>
							</tr>
						</thead>
						<tbody>
							@foreach($cats as $c)
							<tr>
								<th scope="row">{{ $c->id }}</th>
								<td>{{ $c->name }}</td>
								<td>{{ $c->created_at->format('d-m-Y') }}</td>
								<td>
									<a href="{{ route('category.show',$c->id) }}" class="btn btn-primary">Show</a>
									<a href="{{ route('category.edit',$c->id) }}" class="btn btn-warning">Edit <i class="fa far far-pencil"></i></a>
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</body>
</html>