<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{ 
    protected $table = 'products';
protected $fillable = [
'name',
'status',
'image',
'category_id',
'desc'
];
}
